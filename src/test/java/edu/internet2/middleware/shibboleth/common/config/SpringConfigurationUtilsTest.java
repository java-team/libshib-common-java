/*
 * Licensed to the University Corporation for Advanced Internet Development, 
 * Inc. (UCAID) under one or more contributor license agreements.  See the 
 * NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The UCAID licenses this file to You under the Apache 
 * License, Version 2.0 (the "License"); you may not use this file except in 
 * compliance with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.common.config;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 *
 */
public class SpringConfigurationUtilsTest extends TestCase {

    @SuppressWarnings("deprecation")
    public void testDurationShort() {
        
        String dur1 = "PT20M";
        String dur2 = "PT8H";
        String dur3 = "PT24H";

        Assert.assertEquals(1200000L, SpringConfigurationUtils.parseDurationToMillis(dur1, dur1, 0));
        Assert.assertEquals(28800000L, SpringConfigurationUtils.parseDurationToMillis(dur2, dur2, 0));
        Assert.assertEquals(86400000L, SpringConfigurationUtils.parseDurationToMillis(dur3, dur3, 0));
    }

    @SuppressWarnings("deprecation")
    public void testDurationLong() {
        
        String dur1 = "P1D";
        String dur2 = "P7D";
        String dur3 = "P1M";
        String dur4 = "P2M";
        String dur5 = "P3M";
        String dur6 = "P1Y";

        Assert.assertEquals(86400000L, SpringConfigurationUtils.parseDurationToMillis(dur1, dur1, 0));
        Assert.assertEquals(86400000L * 7, SpringConfigurationUtils.parseDurationToMillis(dur2, dur2, 0));
        Assert.assertEquals(86400000L * 31, SpringConfigurationUtils.parseDurationToMillis(dur3, dur3, 0));
        Assert.assertEquals(86400000L * 61, SpringConfigurationUtils.parseDurationToMillis(dur4, dur4, 0));
        Assert.assertEquals(86400000L * 92, SpringConfigurationUtils.parseDurationToMillis(dur5, dur5, 0));
        Assert.assertEquals(86400000L * 365, SpringConfigurationUtils.parseDurationToMillis(dur6, dur6, 0));
    }
}