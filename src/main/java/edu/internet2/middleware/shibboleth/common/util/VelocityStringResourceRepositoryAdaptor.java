/*
 * Licensed to the University Corporation for Advanced Internet Development, 
 * Inc. (UCAID) under one or more contributor license agreements.  See the 
 * NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The UCAID licenses this file to You under the Apache 
 * License, Version 2.0 (the "License"); you may not use this file except in 
 * compliance with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.common.util;

import java.lang.reflect.Method;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.apache.velocity.runtime.resource.util.StringResourceRepository;

/**
 * Adaptor class to intercept and route requests from components for the Velocity
 * {@link StringResourceRepository} associated with the particular {@link StringResourceLoader}
 * implementation used by a {@VelocityEngine} instance. This is done via reflection, so that
 * either the legacy "bug fix" copy we included in the library or the up to date version
 * included in newer Velocity versions can be used interchangeably.
 */
public class VelocityStringResourceRepositoryAdaptor {

    /**
     * Constructor.
     */
    private VelocityStringResourceRepositoryAdaptor() {
        
    }

    /**
     * Returns a reference to the default static repository.
     * 
     * @param engine Velocity engine to obtain repository for
     * 
     * @return  the applicable repository
     * @throw VelocityException if the repository cannot be obtained
     */
    public static StringResourceRepository getRepository(VelocityEngine engine) {
        String implClass = engine.getProperty("string.resource.loader.class").toString();
        if (implClass == null) {
            throw new VelocityException("Velocity engine did not contain a string.resource.loader.class property");
        }
        try {
            Method m = Class.forName(implClass).getMethod("getRepository", (Class[]) null);
            return (StringResourceRepository) m.invoke(null, (Object[]) null);
        } catch (Exception e) {
            e.printStackTrace();
            throw new VelocityException("Exception obtaining StringResourceRepository", e);
        }
    }

    /**
     * Returns a reference to the repository stored statically under the specified name.
     * 
     * @param engine Velocity engine to obtain repository for
     * @param name  name of repository to access
     * 
     * @return  the applicable repository
     * @throw VelocityException if the repository cannot be obtained
     */
    public static StringResourceRepository getRepository(VelocityEngine engine, String name) {
        String implClass = engine.getProperty("string.resource.loader.class").toString();
        if (implClass == null) {
            throw new VelocityException("Velocity engine did not contain a string.resource.loader.class property");
        }
        try {
            Method m = Class.forName(implClass).getMethod("getRepository", String.class);
            return (StringResourceRepository) m.invoke(null, name);
        } catch (Exception e) {
            e.printStackTrace();
            throw new VelocityException("Exception obtaining StringResourceRepository", e);
        }
    }
}