/*
 * Licensed to the University Corporation for Advanced Internet Development, 
 * Inc. (UCAID) under one or more contributor license agreements.  See the 
 * NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The UCAID licenses this file to You under the Apache 
 * License, Version 2.0 (the "License"); you may not use this file except in 
 * compliance with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.internet2.middleware.shibboleth.common.config.attribute.filtering.match.saml;

import javax.xml.namespace.QName;

import org.opensaml.xml.util.XMLHelper;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.w3c.dom.Element;

import edu.internet2.middleware.shibboleth.common.attribute.filtering.provider.match.saml.AttributeInMetadataMatchFunctor;
import edu.internet2.middleware.shibboleth.common.config.attribute.filtering.BaseFilterBeanDefinitionParser;

/**
 * Bean definition parser for {@link AttributeInMetadataMatchFunctor}s.
 */
public class AttributeInMetadataMatchFunctorBeanDefinitionParser extends BaseFilterBeanDefinitionParser {

    /** Schema type. */
    public static final QName SCHEMA_TYPE = new QName(SAMLMatchFunctorNamespaceHandler.NAMESPACE,
            "AttributeInMetadata");

    /** {@inheritDoc} */
    protected void doParse(final Element configElement, final BeanDefinitionBuilder builder) {
        super.doParse(configElement, builder);

        boolean flag = true;
        if (configElement.hasAttributeNS(null, "onlyIfRequired")) {
            flag = XMLHelper.getAttributeValueAsBoolean(configElement.getAttributeNodeNS(null, "onlyIfRequired"));
        }
        builder.addPropertyValue("onlyIfRequired", flag);
        
        flag = false;
        if (configElement.hasAttributeNS(null, "matchIfMetadataSilent")) {
            flag = XMLHelper.getAttributeValueAsBoolean(
                    configElement.getAttributeNodeNS(null, "matchIfMetadataSilent"));
        }
        builder.addPropertyValue("matchIfMetadataSilent", flag);
    }

    /** {@inheritDoc} */
    protected Class getBeanClass(final Element arg0) {
        return AttributeInMetadataMatchFunctor.class;
    }

}